# Spotify-Playlist-Download

## Updated :   `02 April 2023`
v1.1.3
- Added a fix for error `links`  and updated API call links
- Added a functionality for new Playlists Name as Directory Name.

### Last Updated :   `08 March 2023`
v1.1.2
- Added error handling for songs that does not return any links which will cause code to continue on
- Added offsetting songs, now you can change the `OFFSET_VARIABLE`  to any songs that was missed

## How To Use


YOUTUBE VIDEO : https://youtu.be/1wGU652jF9g

To clone and run this application, you'll need [python](https://www.python.org/) 
From your command line:

```bash
# Clone this repository if you have git or just download code.

$ git clone https://github.com/surenjanath/Spotify-Playlist-Download.git

# Install requests if you already don't have it.
$ pip install requests

```

## You can find the medium articles here : 
1. [Automate Downloading Bulk Spotify Playlist [ ARTICLE ]](https://surenjanath.medium.com/automating-spotify-playlist-music-download-spotify-free-version-3ca289bf59f7)


## License

MIT

---

> GitHub [@surenjanath](https://github.com/surenjanath) &nbsp;&middot;&nbsp;
> Twitter [@surenjanath](https://twitter.com/surenjanath)
